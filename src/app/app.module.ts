import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { IndexComponent } from './components/index/index.component';
// importacion de la hoja de enrutamiento
import {RouterModule} from '@angular/router'
import { ROUTES } from './app.routes';
import { RegistroComponent } from './components/registro/registro.component';
import { ContenedoresComponent } from './components/contenedores/contenedores.component';
import { AsignacionrutaComponent } from './components/asignacionruta/asignacionruta.component';
// importacion de la hoja de enrutamiento
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    IndexComponent,
    RegistroComponent,
    ContenedoresComponent,
    AsignacionrutaComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES,{useHash:true}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
