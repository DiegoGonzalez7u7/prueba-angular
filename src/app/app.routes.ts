import { Routes } from "@angular/router";
import { IndexComponent } from "./components/index/index.component";
import { RegistroComponent } from "./components/registro/registro.component";

export const ROUTES: Routes = [
    // ruteo para urls vacias
    { path: '', pathMatch: 'full', redirectTo: 'index' },
    { path: '**', pathMatch: 'full', redirectTo: 'index' },
    // ruteo para urls vacias
    
    { path: 'index', component: IndexComponent },
    { path: 'registro', component: RegistroComponent }
]